var storageProvider = require('./storageProvider');
var _ = require('underscore');

function SimpleOnAllowedStrategy(){

	this.apply = function(tweet){
		console.log('SimpleOnAllowedStrategy applied for tweet: ' + tweet.id);
		console.log(JSON.stringify(tweet));

		tweet = _.pick(tweet, 
			'id', 
			'id_str', 
			'text', 
			'user', 
			'in_reply_to_user_id',
			'is_quote_status',
			'entities',
			'retweet_count',
			'quote_count',
			'timestamp_ms'
			);
		tweet.user = _.pick(tweet.user, 'id', 'name', 'screen_name');
		tweet.entities = _.pick(tweet.entities, 'hashtags', 'symbols');

		console.log('------------------ FROM -----------');
		console.log('---- picked tweet = ' + JSON.stringify(tweet));
		console.log('------------------ TO -----------');

		storageProvider.store(tweet);
	};

}

module.exports = new SimpleOnAllowedStrategy();
