var TweetManager = require('./tweetManager');

function StoreInMemoryTweetProcessor(){

  this.process = function(tweet){
    TweetManager.store(tweet);
  };
};

module.exports = new StoreInMemoryTweetProcessor();