function PlaceOrderCommand(){

	var PERCENTAGE_ABOVE_ASK_PRICE = 1.005;
	var AMOUNT_TO_SPEND = 100;
	var CURRENCY_TO_SPEND = "USDT";

	var that = this;

	that.exchangeProvider = undefined;

	this.init = function(exchangeProvider){
		that.exchangeProvider = exchangeProvider;
	}

	this.execute = function(coinToBuy){

		that.exchangeProvider.getTicker(CURRENCY_TO_SPEND, coinToBuy)
			.done(function(answer){
				if(answer.success == true){
					var orderPrice = _calculateOrderPrice(answer.result.Ask);
					var orderAmount = _calculateOrderAmount(orderPrice);

					console.error('Ask = ' + answer.result.Ask +
						'orderPrice = ' + orderPrice +
					 	'orderAmount = ' + orderAmount);

					that.exchangeProvider.getBalance(CURRENCY_TO_SPEND)
						.done(function(answer){
							if(answer.success == true){
								if(answer.result.Currency == CURRENCY_TO_SPEND){
									console.error('Balans is ' + answer.result.Balance);
									if(answer.result.Balance >= AMOUNT_TO_SPEND){
										_placeOrder(coinToBuy, orderPrice, orderAmount)
									}
								}
							}
					});
				}
			});
	}

	function _calculateOrderPrice(lastAskPrice){
		return lastAskPrice * PERCENTAGE_ABOVE_ASK_PRICE;
	}

	function _calculateOrderAmount(orderPrice){
		return (1 / orderPrice) * AMOUNT_TO_SPEND;
	}

	function _placeOrder(coinToBuy, orderPrice, orderAmount){
		var market = CURRENCY_TO_SPEND + '-' + coinToBuy;
		that.exchangeProvider.buyLimit(market, orderAmount, orderPrice)
			.done(function(answer){
				console.error('BUY RESULT = ' + JSON.stringify(answer));
			});
	}
}

// TODO DAVID : zou ik niet beter de factory exporteren ??? 

module.exports = new PlaceOrderCommand();