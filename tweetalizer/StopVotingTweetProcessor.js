var VoteEmitter = require('./VoteEmitter');

function StopVotingTweetProcessor(){

	this.process = function(tweet){
		VoteEmitter.collectVotes(tweet.id);
	};
}

module.exports = new StopVotingTweetProcessor();
