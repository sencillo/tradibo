function Vote(vote, reason, isVetoVote){

   this._vote;
   this._veto = false;
   this._reason;

   this._vote = vote;
   this._reason = reason || "";
   this._veto = isVetoVote;

   this.getVote = function(){
      return this._vote;
   };

   this.isVeto = function(){
      return this._veto;
   }

   this.getReason = function(){
      return this._reason;
   }
};

function VoteFactory(){
  
   this.vote = function(vote, reason){
      return new Vote(vote, reason, false);
   };

   this.veto = function(reason){
      return new Vote(false, reason, true);
   };
}

module.exports = new VoteFactory();
