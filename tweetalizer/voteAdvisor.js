var _ = require('underscore');
var OnAllowedStrategy = require('./simpleOnAllowedStrategy');
var OnDeniedStrategy = require('./simpleOnDeniedStrategy');
var TweetManager = require('./tweetManager');

function VoteAdvisor(){

	this.advise = function(votesAndIds){
		// simple implementation, only skips vetoed tweets

		var tweetId = votesAndIds[0].id;
		var tweet = TweetManager.fetchAndRemove(tweetId);

		var vetoedVotes = _.filter(votesAndIds, function(voteAndId){
			return voteAndId.vote.isVeto();
		});

		var advice = vetoedVotes.length == 0;

		console.log('## spitting out the final advice for this tweet(' + tweetId + ') : ' + advice);

		if(advice){
			console.log('advies is potisief');
			OnAllowedStrategy.apply(tweet);
		} else {
			console.log('advies is netagief');
			OnDeniedStrategy.apply(tweet);
		}
	}
}

module.exports = new VoteAdvisor();
