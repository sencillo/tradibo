var VoteFactory = require('./voteFactory');

function LoggingTweetProcessor(){

   this.process = function(tweet){
      console.log('[LTProc]' + tweet.id + ' - ' + tweet.created_at + ' - ' + tweet.text);
   };
}

module.exports = new LoggingTweetProcessor();
