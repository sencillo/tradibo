// TODO DAVID : inpluggen van alle exchanges

function ExchangeProvider(){
	
	// TODO DAVID : introduce guarded execution (if!_.isUndefined(this.exchange)..)
	this.exchange = undefined;

	this.init = function(exchange){
		this.exchange = exchange;
	};

	/**
	 * @return a promise, once resolved, the promise will be of one of the following formats:

	 * Generic response format (actually the same as the Bittrex response)
	 * - {"success":true,"message":"","result":{"Bid":0.01599056,"Ask":0.01599501,"Last":0.01599056}}
	 * - {"success":false,"message":"INVALID_MARKET","result":null}
	 */
	this.getTicker = function(coin1, coin2){
		return this.exchange.getTicker(coin1, coin2);
	}

	this.getBalance = function(currency){
		return this.exchange.getBalance(currency);
	}

	this.getCurrencies = function(){
		return this.exchange.getCurrencies();
	}

	this.buyLimit = function(market, quantity, rate){
		return this.exchange.buyLimit(market, quantity, rate);
	}

}

module.exports = new ExchangeProvider();

