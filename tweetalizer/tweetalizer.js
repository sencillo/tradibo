var Twit = require('twit');
var _ = require('underscore');
var StoreInMemoryTweetProcessor = require('./storeInMemoryTweetProcessor');
var LoggingTweetProcessor = require('./loggingTweetProcessor');
var CashtagTweetProcessor = require('./cashtagTweetProcessor');
var StopVotingTweetProcessor = require('./stopVotingTweetProcessor');
var VoteCollector = require('./voteCollector');
var UserPredicate = require('./UserPredicate');

// NOTE: bootstrapping
var mongodbStorageProvider = require('./mongodbStorageProvider');
var storageProvider = require('./storageProvider');
storageProvider.init(mongodbStorageProvider);


// TODO DAVID; maak deze configureerbaar alvorens ze ergens te hosten!
var T = new Twit({
  consumer_key:         'fYeRbRxxZF1IMLIiJ8s1lJn7r',
  consumer_secret:      '2KVdfu2q3ptKgh2gcmqEQDEjsTfu55aRcJLwKF4KnoMkXqmusC',
  access_token:         '231804071-ewc2ja7iMf5D9CcSzemLPANr9xL1ONETGW78rCuw',
  access_token_secret:  'CuTQpRC4Us3fzL7rJNSBZwWEtdXH4tqwsxQFnuaLKPX6Q',
  timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
})

var TWEET_PROCESSORS = [LoggingTweetProcessor, StoreInMemoryTweetProcessor, CashtagTweetProcessor, StopVotingTweetProcessor];

// TODO : strategy buy-sell or buy-keep inbouwen en op moment van beslissing via LEES operatie (trainen van ons model)

var stream = T.stream('statuses/filter', { track: 'ICO' })

stream.on('tweet', function (tweet) {
  if(UserPredicate.retain(tweet)){
    _.each(TWEET_PROCESSORS, function(processor){
      processor.process(tweet);
    });  
  }
})

//
// TEST TWEET_PROCESSORS
//
/*
stream.emit('tweet', {
  id: 'david001',
  text: '#ditiseentest #ICO $BTC dit is een test',
  created_at: 'Daarnet'
})
*/
//
// END TEST TWEET_PROCESSORS
//

//
// EXAMPLE : capture tweet on rocket icon
// 

/*
T.get('search/tweets', { q: '🚀 since:2017-10-21', count: 1000 }, function(err, data, response) {
  console.log(data)
})
*/



