var _ = require('underscore');
var VoteFactory = require('./voteFactory');
var VoteEmitter = require('./VoteEmitter');

//
// All *TweetProcessors should implement the same interface
// The tweet is passed through the chain of tweet processors
// The outcome of the processor is an event, they will all act as some kind of voting machine
// EXP: the last tweet processor will just send an end-of-chain command which triggers the VoteCollector, which advises ..
//

var EXCLUDED_CASHTAGS = ['$ETH', '$BTC'];

function CashtagTweetProcessor(){

	this.process = function(tweet){
		var arr = tweet.text.split(" ");
   		var cashtags = _extractCashtags(arr);

		_log(cashtags);

		if(!_vetoWhenTweetContainsMultipleCashtags(cashtags, tweet.id) && 
			!_vetoWhenTweetContainsAnExcludedCashtag(cashtags, tweet.id) && 
			!_vetoWhenTweetDoesNotContainACashtag(cashtags, tweet.id)){
			VoteEmitter.emit(VoteFactory.vote(true, 'Cashtag accepted'), tweet.id);
		}
   };

	function _vetoWhenTweetContainsMultipleCashtags(cashtags, tweetId){
		if(cashtags.length > 1){
   			VoteEmitter.emit(VoteFactory.veto('Tweet contained multiple cashtags'), tweetId);
   			return true;
		}
		return false;
	};

	function _vetoWhenTweetContainsAnExcludedCashtag(cashtags, tweetId){
		var isVetoed = false;
		_.each(cashtags, function(cashtag){
			if(_.contains(EXCLUDED_CASHTAGS, cashtag)){
				isVetoed = true;
				VoteEmitter.emit(VoteFactory.veto('Tweet contained an excluded cashtag'), tweetId);
			}
		});
		return isVetoed;
	}

	function _vetoWhenTweetDoesNotContainACashtag(cashtags, tweetId){
		if(cashtags.length == 0){
   			VoteEmitter.emit(VoteFactory.veto('Tweet did not contain a cashtags'), tweetId);
   			return true;
		}
		return false;	
	}

	function _log(cashtags){
		if(cashtags.length > 0){
			console.log('Tweet contained the following cashtags');
  			cashtags.forEach(function(s){
				console.log(s);
			});	
		} 
	}

	// TODO DAVID : extract and reuse in SimpeOnAllowedStrategy
	function _extractCashtags(arr){
		return _.filter(arr, function(s){
    		return s.startsWith('$');
		});
	}
};

module.exports = new CashtagTweetProcessor();
