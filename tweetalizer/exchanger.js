require('dotenv').config();

var _ = require('underscore');

//
// Test-drives the exchange components
//
var exchangeProvider = require('./exchangeProvider');
var bittrex = require('./bittrexExchange');

exchangeProvider.init(bittrex);

var placeOrderCommand = require('./placeOrderCommand');

/*********
for (var name in exchangeProvider) {
  if (exchangeProvider.hasOwnProperty(name)) {
    console.log('own property (' + 
      name + ') Value: ' + exchangeProvider[name]);
  }
  else {
    console.log('inherited property' + name); // toString or something else
  }
}
**********/

placeOrderCommand.init(exchangeProvider);
placeOrderCommand.execute('BTC');

console.error('');
console.error('NEXT STEPS: ---------------');
console.error('');
console.error('Na elke accepted tweet, trigger ik een analytics fase die gaat checken of er moet gekocht worden');
console.error('');
console.error('');

/*
exchangeProvider.getCurrencies().done(function(answer){
	console.error('############## Currencies on exchange ############');

	_.each(answer.result, function(rez){
		// console.error(rez.Currency);
		if(rez.Currency == 'BTC'){
			console.error(rez.Currency);
		}
		if(rez.Currency == 'EUR'){
			console.error(rez.Currency);
		}
		if(rez.Currency == 'USDT'){
			console.error(rez.Currency);
		}
	});

	console.error('##################################################');
});
*/

/*
console.log("### ### GET THE BALANCE IN USDT ### ###");
exchangeProvider.getBalance('USDT')
	.done(function(answer){
		console.error('Promise Balance iN USDT = ' + JSON.stringify(answer));
	});
*/


/*
console.log("### ### CHECK IF TRADING PAIR IS AVAILABLE ### ###");
console.log('### exchangeProvider querying for BTC-LTC ticker ###');

exchangeProvider.getTicker('BTC','LTC').done(function(answer){
	console.error('Promise result : ' + JSON.stringify(answer));
});

exchangeProvider.getTicker('BTC','DAVIDCOIN').done(function(answer){
	console.error('Promes result = ' + JSON.stringify(answer));
});
*/



/*
the answer is {"success":true,"message":"","result":{"Bid":0.00862,"Ask":0.00864999,"Last":0.00862}}
the answer is {"success":false,"message":"INVALID_MARKET","result":null}
*/