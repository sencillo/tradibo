// 
// Basic 'always true' user predicate
//

function UserPredicate(){

	this.retain = function(tweet){
		return true;
	}
};

module.exports = new UserPredicate();
