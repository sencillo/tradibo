var EventEmitter = require('./tradiboEventEmitter');
var VoteAdvisor = require('./voteAdvisor');
var _ = require('underscore');

//
// Collects the votes and arranges them per tweet.id
// When the voting process for a given tweet.id is finished, the collector will pass them to the VoteAdvisor
//
function VoteCollector(){

	var that = this;

	that.votes = [];

	EventEmitter.on('vote', _vote);

	EventEmitter.on('collect-votes', _collectVotes)

	function _vote(tweetId, vote){
		that.votes.push({
			id: tweetId,
			vote: vote
		});
	}

	function _collectVotes(tweetId){
		var partitioned = _.partition(that.votes, function(elem){
			return elem.id === tweetId;
		});

		that.votes = partitioned[1];

		var collectedVotes = partitioned[0];
		VoteAdvisor.advise(collectedVotes);
	}
}

module.exports = new VoteCollector();
