var _ = require('underscore');

function TweetManager(){

  this.tweets = [];

  this.store = function(tweet){
  	this.tweets.push(tweet);
  }

  this.fetchAndRemove = function(tweetId){
  	var tweet = _.findWhere(this.tweets, {id: tweetId}); 
  	this.tweets = _.reject(this.tweets, function(tweet){tweet.id == tweetId});
  	return tweet;
  } 
}

module.exports = new TweetManager();
