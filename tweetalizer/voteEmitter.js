var EventEmitter = require('./tradiboEventEmitter');

function VoteEmitter(){

	this.emit = function(vote, tweetId){
		EventEmitter.emit('vote', tweetId, vote);		
	}

	this.collectVotes = function(tweetId){
		EventEmitter.emit('collect-votes', tweetId);
	}
};

module.exports = new VoteEmitter();