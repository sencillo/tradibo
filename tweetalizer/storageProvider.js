// TODO DAVID : requires an implemention
function StorageProvider(){

	this.storageProvider;

	this.init = function(storageProviderImpl){
		console.error('SP.init invoked');
		this.storageProvider = storageProviderImpl;
	}

	this.store = function(tweet){
		console.error('SP.store invoked');
		this.storageProvider.store(tweet);
	}

}

module.exports = new StorageProvider();
