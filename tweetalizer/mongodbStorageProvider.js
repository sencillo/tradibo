//
// Based on the original MongoDB docker image
// Tooling by MongoChef
// 

var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var assert = require('assert');

// TODO DAVID : retrieve configurations from external
var HOST = 'localhost';
var PORT = '32809';
var DATABASE = 'tradingbot';

var MONGO_URL = 'mongodb://' + HOST + ':' + PORT + '/' + DATABASE;
// var COLLECTION_ID = new ObjectID('58d69ef2e40d0dfecac2835c');

function MongodbStorageProvider(){

	function _connect(){
		MongoClient.connect(MONGO_URL, function (err, db) {
			assert.equal(null, err);
			console.log("Connected correctly to server");

			return db;

			// var indexes = db.collection("indexes");
			// index.save({foemp:"baar"});
		});
	}



	this.store = function(tweet){
		console.error('### Storing tweet in mongodb');
		
		// TODO DAVID : optimize connection and db (re)use

		MongoClient.connect(MONGO_URL, function (err, db) {
			
			assert.equal(null, err);
			console.log("Connected correctly to server");

			var tweets = db.collection('tweets');
			tweets.save(tweet);
			db.close();
		});

		
		
	}
}

module.exports = new MongodbStorageProvider();
