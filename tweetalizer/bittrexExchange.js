var request = require('request');
var rp = require('request-promise');
var _ = require('underscore');
var bittrex = require('node-bittrex-api');
var Promise = require('promise');
var HMAC = require('crypto-js/hmac-sha512');
var nonce = require('nonce')();

// 
// API docs: https://bittrex.com/home/api
// NODE package: https://www.npmjs.com/package/node.bittrex.api
//               https://github.com/request/request
//               https://github.com/request/request-promise
// 
// https://bittrex.com/api/{version}/{method}?param=value
//
var API_ROOT = 'https://bittrex.com/api/v1.1/';
var GET_TICKER_ACTION = 'public/getticker';
var GET_BALANCE_ACTION = 'account/getbalance';
var GET_CURRENCIES = 'public/getcurrencies';
var BUY_LIMIT ='market/buylimit';

function BittrexExchange(){

	// STATIC-ALIKE configuration
	bittrex.options({
  		'apikey' : process.env.BITTREX_API_KEY,
  		'apisecret' : process.env.BITTREX_API_SECRET,
  		'verbose' : true,
  		'cleartext' : false
	});

	//
	// SAMPLE CALL https://bittrex.com/api/v1.1/public/getticker?market=BTC-LTC
	//

	this.getTicker = function(coin1, coin2){
		var promise = new Promise(function (resolve, reject) {
			var marketValue = coin1 + '-' + coin2;
			var parameters = [{key: 'market', value: marketValue}];
			_get(GET_TICKER_ACTION, parameters, false, function(answer){
					resolve(_transformTickerResult(answer));
					// TODO DAVID: omzetten, maar ook reject nog meegeven! amai
			});
		});
		return promise;
	}

	this.getCurrencies = function(){
		var promise = new Promise(function (resolve, reject) {
			_get(GET_CURRENCIES, [], false, function(answer){
				resolve(answer);
			});
		});
		return promise;
	}

	this.getBalance = function(currency){
		var promise = new Promise(function(resolve, reject){

			// TODO DAVID : api key en nonce toevoegen in _get(true) gedeelte
			var parameters = [
				{key: 'currency', value: currency},
				{key: 'apikey', value: process.env.BITTREX_API_KEY},
				{key: 'nonce', value: nonce()}
			];

			console.error('&&&&&& GET BALANCE QUERY PARAMS = ' + JSON.stringify(parameters));

			_get(GET_BALANCE_ACTION, parameters, true, function(answer){
				console.log('--- getBalance response data = ' + answer);
				console.log('--- getBalance response err = ' + answer);
				resolve(answer); // TODO DAVID : reject or resolve
			});

			/*
			bittrex.getBalance({currency: currency}, function(data, err){
				console.log('--- getBalance response data = ' + data);
				console.log('--- getBalance response err = ' + data);
				resolve(data); // TODO DAVID : reject or resolve
			});
			*/
		});
		return promise;
	}

	this.buyLimit = function(market, quantity, rate){
		var promise = new Promise(function(resolve, reject){
			
			var parameters = [
				{key: 'market', value: market},
				{key: 'apikey', value: process.env.BITTREX_API_KEY},
				{key: 'quantity', value: quantity},
				{key: 'rate', value: rate}
			];

			console.error('Placing fake order for market ' + market 
				+ ' and quantity ' + quantity + ' and rate ' + rate);
/*
			_get(BUY_LIMIT, parameters, true, function(answer){
				console.log('--- buy limit response data = ' + answer);
				resolve(answer); // TODO DAVID : reject or resolve
			});
			*/

		});
		return promise;
	}

	// NOTE : the Bittrex format is equal to our internal format, so no further transformations are required
	function _transformTickerResult(answer){
		console.log('the untransformed answer from Bittrex is ' + JSON.stringify(answer));
		return answer;
	}

	// TODO DAVID : return promise? or pass-through the callback?
	function _get(action, parameters, requiresAuth, successCallback){

		// if(requiresAuth) --> extra parameters toevoegen apikey,nonce

		var url = API_ROOT + action + _convertKVPairsToQueryString(parameters);

		if(requiresAuth){
			var sign = HMAC(url, process.env.BITTREX_API_SECRET);
			console.error('SIGNED ------> ' + sign);
/*
		$apikey='xxx';
$apisecret='xxx';
$nonce=time();
$uri='https://bittrex.com/api/v1.1/market/getopenorders?apikey='.$apikey.'&nonce='.$nonce;
$sign=hash_hmac('sha512',$uri,$apisecret);
$ch = curl_init($uri);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('apisign:'.$sign));
$execResult = curl_exec($ch);
$obj = json_decode($execResult);
		*/
		}

		// if(!requiresAuth){

			console.error('## REQUEST URL = ' + url);

			var headers = {'User-Agent': 'Request-Promise'};

			if(requiresAuth){
				headers.apisign = sign;
				console.error('AUTH HEADERS ---> ' + JSON.stringify(headers));
			}

			// TODO DAVID : zie https://github.com/request/request-promise GET SOMETHING FROM REST - querystring anders mappen
			var options = {
    			uri: url,
    			headers: headers,
    			json: true // Automatically parses the JSON string in the response
			};

			rp(options)
    			.then(successCallback)
    			.catch(function (err) {
    				console.error('### ERROR --> ' + err);
    			});

		// } 
	}

	function _convertKVPairsToQueryString(parameters){
		if(!_.isArray(parameters)){
			return '';
		}

		var result = '?';
		_.each(parameters, function(parameter){
			result = result + parameter.key + '=' + parameter.value + '&';
		});
		result = result.slice(0, -1);
		return result;
	}

}

module.exports = new BittrexExchange();
