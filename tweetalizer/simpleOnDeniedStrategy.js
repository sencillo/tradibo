function SimpleOnDeniedStrategy(){

	this.apply = function(tweet){
		console.log('SimpleOnDeniedStrategy applied for tweet: ' + tweet.id);
		// console.log(JSON.stringify(tweet));
	};
}

module.exports = new SimpleOnDeniedStrategy();
